import configparser
import json
import sys
from datetime import datetime
from os.path import abspath, dirname, join

import aiohttp
import discord
import sentry_sdk
from discord.ext import commands

from cogs.utils import prompt
from _version import __version__
from constants import Constants, __botname__, __description__
from db.manager import DatabaseManager
from utilities.helpers import Helpers
from utilities.tasks import Tasks

curdir = join(abspath(dirname(__file__)))
parentdir = join(curdir, "../")

botconfig = configparser.ConfigParser()
try:
    botconfig.read(join(parentdir, "../botconfig.ini"))
    # Tests the path
    test = botconfig.get("BotConfig", "DSN")
except configparser.NoSectionError:
    botconfig.read(join(parentdir, "botconfig.ini"))

# Gets the values
__dsn__ = botconfig.get("BotConfig", "DSN")
__token__ = botconfig.get("BotConfig", "DISCORDTOKEN")
try:
    __environment__ = botconfig.get("BotConfig", "ENVIRONMENT")
except Exception:
    __environment__ = "Development"

initial_extensions = (
    # Misc commands
    # Disabled key as we're not handing out keys at the moment and likely won't
    # "cogs.misc.key",
    "cogs.misc.gtfo",
    "cogs.misc.ping",
    "cogs.misc.prefix",
    "cogs.misc.bughunter",
    "cogs.misc.say",
    # Admin commands
    "cogs.admintools.shutdown",
    "cogs.admintools.info",
    "cogs.admintools.stats",
    # Other commands
    "cogs.admin",
    # Events
    "utilities.events",
)

# Create the Discord Intents
# https://discordpy.readthedocs.io/en/latest/api.html#discord.Intents
# https://discordpy.readthedocs.io/en/latest/intents.html
intents = discord.Intents.all()


def _prefix_callable(bot, msg):
    # Always allow mentioning the bot as a prefix
    base = [f"<@!{bot.user.id}> ", f"<@{bot.user.id}> "]
    # If in a guild, add the prefixes
    if msg.guild:
        bot.log.debug(f"PrefixCall: Guild found: {msg.guild.id}")
        try:
            settings = bot.guild_settings.get(msg.guild.id)
            if not settings:
                bot.log.debug(f"PrefixCall: No settings for {msg.guild.id}")
                return base
            bot.log.debug(f"PrefixCall: {msg.guild.id} | {settings.bot_prefix}")
            if settings.bot_prefix:
                base.extend(settings.bot_prefix)
        except (KeyError, AttributeError) as error:
            bot.log.warning(
                f"PrefixCall: Key/Att error. {error.__class__.__name__}:** {error}"
            )
            pass

    return base


class Bot(commands.AutoShardedBot):
    def __init__(self, log):
        super().__init__(
            command_prefix=_prefix_callable,
            description=__description__,
            help_command=commands.DefaultHelpCommand(dm_help=True),
            case_insensitive=True,
            fetch_offline_members=True,
            max_messages=20000,
            intents=intents,
        )
        self.database = None
        self.version = __version__
        self.log = log
        self.log.info("/*********Starting App*********\\")
        self.log.info(f"App Name: {__botname__} | Version: {__version__}")
        self.started_time = datetime.utcnow()
        self.botconfig = botconfig
        self.constants = Constants()
        self.owner = None
        self.guild_settings = {}
        self.session = aiohttp.ClientSession(loop=self.loop)
        self.log.info(f"Initialized: Session Loop")
        self.database = DatabaseManager(self.botconfig)
        self.log.info(f"Initialized: Database Manager")
        self.prompt = prompt.Prompt(self)
        self.log.info(f"Initialized: Prompt")
        self.gtfo_guild = None
        self.gtfo_role_id = int(botconfig.get("BotConfig", "ROLE_ID"))
        self.mailchimp_api_key = botconfig.get("BotConfig", "MAILCHIMP_API_KEY")
        self.mailchimp_list_id = botconfig.get("BotConfig", "MAILCHIMP_LIST_ID")
        # Loading helpers after Mailchimp due to dependencies.
        self.helpers = Helpers(self)
        self.log.info(f"Initialized: Helpers")
        self.tasks = Tasks(self)
        self.log.info(f"Initialized: Tasks")

        # Sets up the sentry_sdk integration:
        sentry_sdk.init(dsn=__dsn__, release=__version__, environment=__environment__, before_send=self.sentry_before_send,)
        self.log.debug(f"Initialized: Sentry SDK")

        for extension in initial_extensions:
            try:
                self.load_extension(extension)
                self.log.info(f"Loaded extension {extension}")
            except Exception as err:
                self.log.exception(
                    f"Failed to load extension {extension}. {sys.exc_info()[0].__name__}: {err}"
                )

    # Perform any actions to the event prior to it being sent.
    def sentry_before_send(self, event, hint):
        # Adding in the Bot Name and Bot ID to make identifying which bot the issue is on
        event.setdefault("tags", {})["Bot_Name"] = f"{self.user if self.user else None}"
        event.setdefault("tags", {})[
            "Bot_ID"
        ] = f"{self.user.id if self.user else None}"
        return event

    def get_guild_prefixes(self, msg, *, local_inject=_prefix_callable):
        return local_inject(self, msg)

    async def remove_guild_prefixes(self, session, guild_id, oldprefix, settings):
        try:
            # Remove the prefix
            settings.bot_prefix.remove(oldprefix)
            # Convert to string for storing in the database
            settings.bot_prefix = json.dumps(settings.bot_prefix)
            session.commit()

            # Update local cache
            self.guild_settings[guild_id] = await self.helpers.get_one_guild_settings(
                session, guild_id
            )
        except Exception as err:
            self.log.exception(
                f"Error removing guild prefix for {guild_id}. {sys.exc_info()[0].__name__}: {err}"
            )
            session.rollback()

    async def add_guild_prefixes(self, session, guild_id, newprefix, settings):
        try:
            # Remove the prefix
            settings.bot_prefix.append(newprefix)
            # Convert to string for storing in the database
            settings.bot_prefix = json.dumps(settings.bot_prefix)
            session.commit()

            # Update local cache
            self.guild_settings[guild_id] = await self.helpers.get_one_guild_settings(
                session, guild_id
            )
        except Exception as err:
            self.log.exception(
                f"Error getting adding guild prefix for {guild_id}. {sys.exc_info()[0].__name__}: {err}"
            )
            session.rollback()

    async def on_command_error(self, ctx, error):
        if isinstance(error, commands.NoPrivateMessage):
            await ctx.author.send("This command cannot be used in private messages.")
        elif isinstance(error, commands.DisabledCommand):
            await ctx.author.send("Sorry. This command is disabled and cannot be used.")
        elif isinstance(error, commands.CommandInvokeError):
            self.log.exception(
                f"In {ctx.command.qualified_name}: {error.original.__class__.__name__}: {error.original}"
            )
        elif isinstance(error, commands.UserInputError):
            await ctx.author.send(
                f"Sorry. There was a user error processing command: **{ctx.command.qualified_name}**.\n\n**{error.__class__.__name__}:** {error}"
            )
        elif isinstance(error, commands.BadArgument):
            await ctx.author.send(
                f"Sorry. There was a bad argument error processing command: **{ctx.command.qualified_name}**.\n\n**{error.__class__.__name__}:** {error}"
            )
        # Responds to user if _the user_ is missing permission to use the command
        elif isinstance(error, commands.MissingPermissions):
            await ctx.author.send(f"**{error.__class__.__name__}:** {error}")
        # Responds to user if _the bot_ is missing permission to use the command
        elif isinstance(error, commands.BotMissingPermissions):
            await ctx.author.send(f"**{error.__class__.__name__}:** {error}")
        # Checks if user is listed as a Bot Owner
        elif isinstance(error, commands.NotOwner):
            await ctx.author.send(
                f"**{error.__class__.__name__}:** {error} This command is only for bot owners."
            )

    async def on_ready(self):
        # Gets all guild settings:
        self.helpers.get_all_guild_settings()
        # Starts all tasks
        await self.tasks.start_tasks()
        self.log.info(f"Started all Tasks")

        # Load the mod mail
        try:
            self.load_extension("utilities.modmail")
            self.log.info(f"Loaded extension utilities.modmail")
        except (commands.ExtensionAlreadyLoaded, commands.errors.ExtensionNotFound):
            pass

        # Sets bot owner information
        app_info = await self.application_info()
        try:
            if app_info.team:
                self.owner = app_info.team.members
                self.owner_ids = {member.id for member in app_info.team.members}
                self.log.info(f"AppInfo found Team, so setting for team of owners.")
            else:
                self.owner = app_info.owner
                self.owner_id = app_info.owner.id
                self.log.info(f"AppInfo is missing Team, so setting for single owner.")
        except Exception as err:
            self.log.exception(
                f"Failed to set bot owner information. {sys.exc_info()[0].__name__}: {err}"
            )

        # Sets the GTFO guild
        try:
            self.gtfo_guild = self.get_guild(
                int(botconfig.get("BotConfig", "GUILD_ID"))
            )
            self.bughunter_guild = self.get_guild(
                int(botconfig.get("BotConfig", "BUGHUNTER_GUILD_ID"))
            )
        except Exception as err:
            self.log.exception(
                f"Failed to get GTFO Guild or Bug Hunter Guild. {sys.exc_info()[0].__name__}: {err}"
            )

        # Sets activity status
        try:
            if __environment__ == "Development":
                await self.change_presence(
                    activity=discord.Game(name=f"In Dev v{__version__}")
                )
        except Exception as err:
            self.log.exception(
                f"Failed to set presence info. {sys.exc_info()[0].__name__}: {err}"
            )

        # All ready
        self.log.info(f"Ready: {self.user} ({self.user.id})")

    async def on_resumed(self):
        self.log.debug("Resumed Discord session...")

    async def on_error(self, event_method, *args, **kwargs):
        self.log.exception(
            f"An error was caught. Event Method: {event_method}. {sys.exc_info()[0].__name__}: {sys.exc_info()[1]}"
        )

    async def close(self):
        self.log.info("Caught Keyboard Interrupt. Gracefully closing sessions.")
        # Close database manager
        if self.database:
            self.database.close_engine()
        # Close the bot
        await super().close()
        # Close the core session keeping bot alive
        await self.session.close()

    def run(self):
        super().run(__token__, reconnect=True)
