import os
import sys
from datetime import datetime

import discord
import psutil
from discord.ext import commands


class Info(commands.Cog):
    """Provides information about the bot including an invite link, source code,
    memory and cpu usage, etc.

    Requires Permission: Manage Guild
    """

    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=["invite"])
    @commands.has_permissions(send_messages=True)
    @commands.guild_only()
    async def info(self, ctx):
        """Provides information about the bot including an invite link, source code,
        memory and cpu usage, etc.

        Requires Permission
        -------------------
        Send Messages
        """
        try:
            self.bot.log.info(
                f"CMD {ctx.invoked_with} called by {ctx.message.author} ({ctx.message.author.id})"
            )

            # Get bot uptime
            uptime = self.bot.helpers.relative_time(
                datetime.utcnow(), self.bot.started_time, brief=True
            )

            # Get system usage
            process = psutil.Process(os.getpid())
            cpu_usage = process.cpu_percent() / psutil.cpu_count()
            memory_usage = process.memory_full_info().uss / 1024 ** 2

            # Create the embed of info
            embed = discord.Embed(
                color=discord.Color.blurple(),
                title="Bot Invite Link & Info",
                timestamp=datetime.utcnow(),
            )
            # Gets the bot owner info.
            if isinstance(self.bot.owner, list):
                owners = [
                    f"{temp_user.mention} ({temp_user.id})"
                    for temp_user in self.bot.owner
                ]
            else:
                owners = [f"{self.bot.owner.mention} ({self.bot.owner.id})"]
            owners_string = ", ".join(owners)
            embed.add_field(name="Bot Owner", value=f"{owners_string}", inline=False)
            embed.add_field(
                name="Bot Version", value=f"{self.bot.version}", inline=True
            )
            embed.add_field(
                name="# of Guilds", value=f"{len(self.bot.guilds)}", inline=True
            )
            embed.add_field(
                name="Memory Usage", value=f"{memory_usage:.2f} MiB", inline=True
            )
            embed.add_field(name="CPU Usage", value=f"{cpu_usage:.2f}%", inline=True)
            embed.add_field(name="Bot Uptime", value=f"{uptime}", inline=True)

            await ctx.send(embed=embed)
        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing {ctx.command}. Error has already been reported to my developers."
            )
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing {ctx.command}. Error has already been reported to my developers."
            )


def setup(bot):
    bot.add_cog(Info(bot))
