import sys
from discord.ext import commands
import discord


class Stats(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.has_permissions(send_messages=True)
    @commands.guild_only()
    async def stats(self, ctx):
        """List some guild statistics.

        Requires Permission: Send Messages.
        """

        self.bot.log.info(
            f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})"
        )
        try:

            guild = ctx.message.guild
            text_channels = guild.text_channels or []
            voice_channels = guild.voice_channels or []

            # In order to get a more accurate Member Count and Presence Count, we need to pull from an invite code
            member_count = guild.member_count
            presence_count = 5000
            # Check if we have perms to get guild invites
            try:
                all_invites = await guild.invites()
                # If the server doesn't have any invites, let's try and create one
                if all_invites is None:
                    await ctx.send(
                        f"There are no invites for this server. Please create a never expiring invite if you want the most accurate stats."
                    )

                else:
                    for tmp_invite in all_invites:
                        invite = await ctx.bot.fetch_invite(
                            tmp_invite.code, with_counts=True
                        )
                        member_count = invite.approximate_member_count
                        presence_count = invite.approximate_presence_count
                        break
            except (discord.errors.Forbidden, Exception):
                await ctx.send(
                    f"I'm currently missing **Manage Guild** permission in order to get the most accurate numbers. Please provide me this permission so I can get the data by using the servers Discord Invites."
                )

            # Create the embed
            embed = discord.Embed(
                title=f"Guild Owner: {guild.owner} ({guild.owner.id})"
            )
            embed.set_author(
                name=f"Statistics for {ctx.message.guild.name} ({ctx.message.guild.id})",
                icon_url=guild.icon_url,
            )
            # This is number of members currently in the guild
            max_members = guild.max_members if guild.max_members else 0
            embed.add_field(
                name="Member Count",
                value=f"{member_count:,} / {max_members:,}",
                inline=True,
            )
            # This is maximum number of presence members, or people currently connected to the guild
            max_presences = guild.max_presences if guild.max_presences else 5000
            embed.add_field(
                name="Presence Count",
                value=f"{presence_count:,} / {max_presences:,}",
                inline=True,
            )
            # Number of text channels
            embed.add_field(
                name="Text Channels", value=f"{len(text_channels)}", inline=True
            )
            # Number of voice users
            embed.add_field(
                name="Active Voice Users",
                value=f"{sum(len(vc.members or []) for vc in voice_channels)}",
                inline=True,
            )
            # Number of voice channels
            embed.add_field(
                name="Active Voice Channels",
                value=f"{len(list(filter(lambda vc: len(vc.members) > 0, voice_channels)))} / {len(voice_channels)}",
                inline=True,
            )
            await ctx.send(embed=embed)

        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing {ctx.command}. Error has already been reported to my developers."
            )
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing {ctx.command}. Error has already been reported to my developers."
            )


def setup(bot):
    bot.add_cog(Stats(bot))
