import sys

import discord
from discord.ext import commands
from sentry_sdk import configure_scope


class GTFO(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_message(self, msg):
        try:
            # If message is from the bot itself, ignore it
            if msg.author.id == self.bot.user.id:
                return

            # Check if it's from a DM, then process it
            if isinstance(msg.channel, discord.DMChannel):
                self.bot.log.info(f"New DM from {msg.author} ({msg.author.id})")
                param1 = msg.content.split(" ")[0].strip()
                for command in self.bot.commands:
                    if param1.startswith(f"!{str(command)}"):
                        return
                # Otherwise assume they are sending us their email
                email = param1
                self.bot.log.info(f"Message is not a known command, proceeding to process from {msg.author} ({msg.author.id})")

                # Get the GTFO Guild and check if it's online
                guild = self.bot.gtfo_guild
                if not await self.bot.helpers.check_guild_avail(guild, msg.author):
                    return await msg.channel.send(
                        f"Oh no, the {guild.name} server seems to be unavailable for the moment. 😐 Please try again in a few minutes, I believe it's not going to last long!"
                    )

                # Get the Ambassador role
                amba_role = guild.get_role(self.bot.gtfo_role_id)
                if not await self.bot.helpers.check_if_amba_role_found(amba_role, guild, msg.author):
                    return await msg.channel.send(
                        f"Whoops, I can't even find the role I'm supposed to give you. 🤔 You shouldn't see this error, please contact an administrator!"
                    )

                # Check if user already has the Ambassador role. First tries to get from local cache, if not uses an API call
                member = guild.get_member(msg.author.id)
                if not member:
                    member = await guild.fetch_member(msg.author.id)
                # Make sure they are in the guild
                if not member:
                    self.bot.log.debug(f"User not found in the guild {guild.name} ({guild.id}). Msg from {msg.author} ({msg.author.id})")
                    return await msg.channel.send(
                        f"Whoops, I'm not able to find you in the GTFO server. Can you make sure you are in the server, you can join here: https://discord.gg/GTFO."
                    )
                # Finally check if they have the role
                if await self.bot.helpers.check_if_user_has_amba_role(member, amba_role):
                    return await msg.channel.send(
                        f"Awesome, it looks like you already have the {amba_role.name} role. 😄 My job here is done, I don't have any more roles to give you, sorry!"
                    )

                # Now let's check to make sure it's a valid email
                if not email or not self.bot.constants.email_regex.match(email):
                    self.bot.log.debug(f"Email '{email}' not valid. Msg from {msg.author} ({msg.author.id})")
                    return await msg.channel.send(
                        "I was unable to find a valid email address. Please make sure you are only sending the email you used to sign up for the Ambassador newsletter. If you need assistance please contact <@!553357041586929704> and our mod team can assist.")

                # Now let's check if they are subscribed to the newsletter
                mc_status = await self.bot.helpers.check_mc_is_subscribed(email, msg)
                if mc_status is False:
                    self.bot.log.debug(f"User has not finished subscribing to MailChimp list. Email: '{email}'. Msg from {msg.author} ({msg.author.id})")
                    return await msg.channel.send(f"Sorry, but I'm not showing you as registered to the Ambassador newsletter.\n\n"
                                                  f"Please make sure you have confirmed the subscription in the verification email you received. The email may be in your junk or spam folder.\n\n"
                                                  f"If you have done all those steps, I might not be able to see the subscription yet. Please wait 10 minutes then try again. If you are still having issues please contact <@!553357041586929704> and our mod team can assist. Thank you.")
                elif mc_status is None:
                    self.bot.log.debug(
                        f"Email not found in MailChimp list. Email: '{email}'. Msg from {msg.author} ({msg.author.id})"
                    )
                    return await msg.channel.send(
                        f"Sorry, but I'm not finding your email in the Ambassador newsletter list. Please make sure you are using the correct email address you registered with, otherwise you can go to <https://gtfothegame.com> and click the 'Become a 10 Chambers Ambassador' button.")
                # Last, let's add the role to them.
                try:
                    await member.add_roles(amba_role, reason="Granting role after newsletter verification", atomic=True)
                    self.bot.log.debug(f"Added role {amba_role.name} in the guild {guild.name} for {msg.author} ({msg.author.id})")
                    return await msg.channel.send(
                        f"Congratulations, you confirmed your subscription and now have the {amba_role.name} role. Now is the time to engage with our community! Thanks a lot for flying with us, enjoy the game!")
                except Exception as err:
                    with configure_scope() as scope:
                        scope.set_extra("email", email)
                        scope.set_extra("d_user_id", msg.author.id)
                        scope.set_extra("d_msg_id", msg.id)
                    self.bot.log.exception(f"Error adding the role to the user. {sys.exc_info()[0].__name__}: {err}")
                    return await msg.channel.send(
                        f"I've ran into a problem adding the {amba_role.name} role for you. Please wait 10 minutes then try again. If you are still having issues please contact <@!553357041586929704> and our mod team can assist.")

        except Exception as err:
            with configure_scope() as scope:
                scope.set_extra("d_user_id", msg.author.id)
                scope.set_extra("d_msg_id", msg.id)
            self.bot.log.exception(f"Generic error processing GTFO Module Message Event. {sys.exc_info()[0].__name__}: {err}")


def setup(bot):
    bot.add_cog(GTFO(bot))
