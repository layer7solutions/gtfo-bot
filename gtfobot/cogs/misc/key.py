import io
import sys
from datetime import datetime

import discord
from discord.ext import commands
from sentry_sdk import configure_scope
from sqlalchemy import case, func, true, false
from sqlalchemy.exc import DBAPIError, IntegrityError

from db import models
from utilities.helpers import has_guild_permissions


class Key(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.dm_only()
    @commands.group(invoke_without_command=True, hidden=True)
    async def key(self, ctx, email: str = None, category: str = 'Default'):
        """Gets a product key for specified category. If you are unsure what the category is please ask a mod or admin.

        Requires to be in a DM and that the prefix is used.

        Example:
        !key email@example.com
        !key email@example.com category
        """

        # Disabling the key command until next testing
        # TO DO - A proper enable/disable campaign command

        session = self.bot.helpers.get_db_session()
        try:
            self.bot.log.info(
                f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})"
            )

            if not email:
                return await ctx.send(
                    f"Please provide the email you used when registering for the Ambassador program."
                )
            # Set meta data
            user = ctx.message.author
            guild = self.bot.gtfo_guild
            if not guild:
                return await ctx.send(
                    f"Oh no, the GTFO server seems to be unavailable for the moment. 😐 Please try again in a few minutes, I believe it's not going to last long!"
                )
            # If they enter the category as "beta" then set it to "alpha" so that the user doesn't get a key twice, but allows visual adjustment.
            category = category.strip().lower()
            if category == "beta" or category == "default":
                category = "alpha"
            # If the category is alpha/beta (the testing keys) then don't let them get a key
            if category == "alpha":
                return await ctx.send(
                    f"Thank you for your interest in testing GTFO, however all testing has concluded. **Early Access will be releasing December 9th at Noon Pacific**. We hope to see you join us then.")
            # check if user is in the bug hunter server
            temp_bughunter_members_ids = [tmp_member.id for tmp_member in
                                          self.bot.bughunter_guild.members]
            if user.id not in temp_bughunter_members_ids:
                return await ctx.send(f"Sorry, but you are not allowed to use this command at this time. You must be in BH to use this command.")
            # Get the DB profile for the user
            db_user = await self.bot.helpers.db_get_user(session, user.id)

            # Validate it's a proper email
            if not email or not self.bot.constants.email_regex.match(email):
                self.bot.log.debug(f"Email '{email}' not valid. Msg from {user} ({user.id})")
                return await ctx.send(
                    "I was unable to find a valid email address. Please make sure you are only sending the email you used to sign up for the Ambassador newsletter. If you need assistance please contact <@!553357041586929704> and our mod team can assist.")

            # Validate they have a MailChimp Ambassador Subscription
            mc_status = await self.bot.helpers.check_mc_is_subscribed(email, ctx.message)
            if mc_status is False:
                self.bot.log.debug(
                    f"User has not finished subscribing to MailChimp list. Email: '{email}'. Msg from {user} ({user.id})"
                )
                return await ctx.send(
                    f"Sorry, but I'm not showing you as registered to the Ambassador newsletter.\n\n"
                    f"Please make sure you have confirmed the subscription in the verification email you received. The email may be in your junk or spam folder.\n\n"
                    f"If you have done all those steps, I might not be able to see the subscription yet. Please wait 10 minutes then try again. If you are still having issues please contact <@!553357041586929704> and our mod team can assist. Thank you."
                )
            elif mc_status is None:
                self.bot.log.debug(
                    f"Email not found in MailChimp list. Email: '{email}'. Msg from {user} ({user.id})"
                )
                return await ctx.send(
                    f"Sorry, but I'm not finding your email in the Ambassador newsletter list. Please make sure you are using the correct email address you registered with, otherwise you can go to <https://gtfothegame.com> and click the 'Become a 10 Chambers Ambassador' button."
                )

            # Check if they have the Ambassador role and if not give it to them
            async def get_member(usr):
                mbr = guild.get_member(usr.id)
                if mbr:
                    return mbr
                else:
                    mbr = await guild.fetch_member(usr.id)
                    if mbr:
                        return mbr
                    else:
                        return None

            try:
                amba_role = guild.get_role(self.bot.gtfo_role_id)
                if await self.bot.helpers.check_if_amba_role_found(amba_role, guild, user):
                    # Check if user is in the server
                    member = await get_member(user)
                    if member:
                        # If user doesn't already have the amba role, add it
                        if not await self.bot.helpers.check_if_user_has_amba_role(
                                member, amba_role
                        ):
                            await member.add_roles(
                                amba_role,
                                reason="Granting role after newsletter verification",
                                atomic=True,
                            )
                            self.bot.log.debug(
                                f"Added role {amba_role.name} in the guild {guild.name} for {member} ({member.id})"
                            )
            except Exception as err:
                self.bot.log.exception(f"Error adding the Ambassador role. {sys.exc_info()[0].__name__}: {err}")

            # Check if they've already redeemed a key for this campaign by user+guild+campaign
            unq_user_guild_cat_result = (
                session.query(models.Keys)
                    # Links the User and Keys table to get the User
                    .join(models.User, models.User.id == models.Keys.user_id)
                    # Links the Server and Keys table to get the Guild
                    .join(models.Server, models.Server.id == models.Keys.server_id)
                    # Filters on the user where the users Discord ID matches
                    # And where the guilds Discord ID matches
                    .filter(
                    models.User.discord_id == user.id,
                    models.Server.discord_id == guild.id,
                    models.Keys.category == category,
                )
            ).first()

            # Check if they've already redeemed a key for this campaign by email+guild+campaign
            unq_email_guild_cat_result = (
                session.query(models.Keys)
                    # Links the User and Keys table to get the User
                    .join(models.User, models.User.id == models.Keys.user_id)
                    # Links the Server and Keys table to get the Guild
                    .join(models.Server, models.Server.id == models.Keys.server_id)
                    # Filters on the user where the users Discord ID matches
                    # And where the guilds Discord ID matches
                    .filter(
                    models.Keys.email == email,
                    models.Server.discord_id == guild.id,
                    models.Keys.category == category,
                )
            ).first()

            # If the user+guild+category has already received a key, don't let them get another
            if unq_user_guild_cat_result and unq_user_guild_cat_result.redeemed:
                self.bot.log.warning(
                    f"The same user {user} ({user.id}) is potentially trying to get multiple keys with same or different email address"
                )
                return await ctx.send(
                    f"Sorry, but only one key is allowed per campaign per user.\n\nIf you have any questions please contact <@!553357041586929704>."
                )

            # If the email+guild+category has already received a key, don't let them get another
            if unq_email_guild_cat_result and unq_email_guild_cat_result.redeemed:
                self.bot.log.warning(
                    f"A different user {user} ({user.id}) is potentially trying to get multiple keys with a different email address '{email}'"
                )
                return await ctx.send(
                    f"Sorry, but a key has already been redeemed for that user.\n\nIf you have any questions please contact <@!553357041586929704>."
                )

            # If they haven't received a key for this campaign, let's assign them one
            # Get a random Redis value (product key) associated with the set (category) and removes from the set. Returns none if null
            product_key = self.bot.helpers.redis.spop(f"{category.lower()}_{guild.id}")
            # If no product key found, check if they got the category right
            if not product_key:
                cat = (
                    session.query(models.Keys.id)
                        # Links the Server and Keys table to get the Guild
                        .join(
                        models.Server, models.Server.id == models.Keys.server_id
                    ).filter(
                        models.Server.discord_id == guild.id,
                        models.Keys.category == category,
                    )
                ).first()
                # If no category found, let them know to try again
                if not cat:
                    return await ctx.send(
                        f"Sorry, I'm not able to find a category called '{category}'. Please check to make sure you're using the correct category and reach out to the mod team at <@!553357041586929704> if you still need assistance."
                    )

                # If they did get the category right, let them know we're out of keys or none found in Redis cache
                self.bot.log.warning(f"All out of Product Keys or None Found.")
                channel = discord.utils.get(
                    guild.text_channels, name="key-logs"
                )
                if channel:
                    await channel.send(
                        f"Hello mods and <@!82942340309716992>. '{user}' ({user.id}) has requested a key however we are currently all out of product keys or bot's unable to find one in the Redis cache for the **{category if category else 'default'}** campaign. Please let the devs know. Thanks."
                    )
                return await ctx.send(
                    f"Sorry, we've ran out of product keys. I've already notified the developers. Hopefully we'll have more soonTM."
                )

            # Now that we have the product key, we need to update the database to show that it's been used
            key = (
                session.query(models.Keys)
                    # Links the Server and Keys table to get the Guild
                    .join(models.Server, models.Server.id == models.Keys.server_id).filter(
                    models.Server.discord_id == guild.id,
                    models.Keys.category == category,
                    models.Keys.redeemed == false(),
                    models.Keys.product_key == product_key,
                )
            ).first()

            if key:
                key.redeemed = True
                key.user = db_user
                key.email = email
                session.commit()

                try:
                    msg_to_user = await ctx.send(
                        f"Congratulations! 🎉 Here is your GTFO Product Key: **{product_key}**\n\n"
                        f"To redeem the key, go here: (<https://store.steampowered.com/account/registerkey?key={product_key}>). We hope you enjoy playing, and we look forward to any feedback you have :)\n\n"
                        f"If you have questions please contact <@!553357041586929704> and our mod team can assist."
                    )
                    if msg_to_user:
                        self.bot.log.info(f"Provided product key '{product_key}' from category '{category}' to '{user}' ({user.id})")
                    else:
                        self.bot.log.exception(f"No message object returned from Discord when trying to send the product key to the user '{user}' ({user.id})")
                except Exception as err:
                    self.bot.log.exception(f"Bot tried to send the product key to the user, but ran into an issue. {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}")

                # Log to key-logs channel
                try:
                    if category == "alpha":
                        logs = discord.utils.get(
                            guild.text_channels, name="key-logs"
                        )
                    else:
                        logs = discord.utils.get(
                            self.bot.bughunter_guild.text_channels, name="key-logs"
                        )
                    if logs:
                        # Create the embed of info
                        embed = discord.Embed(color=discord.colour.Color.red(), timestamp=datetime.utcnow(), title="Product Key Issued")
                        embed.set_author(name=f"{user} ({user.id})", icon_url=user.avatar_url)
                        embed.add_field(name="Last 4 of Product Key", value=f"{key.product_key[-4:]}")
                        embed.add_field(name="Category", value=f"{key.category}")
                        await logs.send(embed=embed)
                except Exception as err:
                    self.bot.log.warning(f"Error logging key. {sys.exc_info()[0].__name__}: {err}")
                return

        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error responding to '{ctx.command}' command. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing '{ctx.command}' command. Error has already been reported to my developers."
            )
        except DBAPIError as err:
            self.bot.log.exception(
                f"Error processing database query for '{ctx.command}' command. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing '{ctx.command}' command. Error has already been reported to my developers."
            )
            session.rollback()
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to '{ctx.command}' command via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing '{ctx.command}' command. Error has already been reported to my developers."
            )
        finally:
            session.close()

    @commands.guild_only()
    @has_guild_permissions(manage_channels=True)
    @key.command(hidden=True)
    async def stats(self, ctx):
        """Shows product key stats."""

        session = self.bot.helpers.get_db_session()
        try:
            self.bot.log.info(
                f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})"
            )

            # Set meta data
            guild = ctx.message.guild

            # Get all key data
            result = (
                session.query(
                    func.count(case([(models.Keys.redeemed == true(), true())])),
                    func.count(case([(models.Keys.redeemed == false(), false())])),
                    models.Keys.category,
                )
                    # Links the Server and Keys table to get the Guild
                    .join(models.Server, models.Server.id == models.Keys.server_id)
                    .filter(models.Server.discord_id == guild.id)
                    .group_by(models.Keys.category)
            ).all()

            if not result:
                self.bot.log.warning(
                    f"No keys found for this guild: '{guild}' ({guild.id})."
                )
                return await ctx.send(
                    f"No keys found for this guild: '{guild}' ({guild.id})."
                )

            # Build embed
            embed = discord.Embed(
                title=f"Product Key Stats: {guild} ({guild.id})",
                timestamp=datetime.utcnow(),
                color=discord.Color.blurple(),
            )
            for row in result:
                rcount = self.bot.helpers.redis.scard(f"{row.category.lower()}_{guild.id}")
                embed.add_field(
                    name=f"**Category:** {row.category}",
                    value=f"**Used:** {row[0]}\n**Free:** {row[1]}\n**Redis Free:** {rcount}\n**Total:** {row[0] + row[1]}",
                    inline=True,
                )

            return await ctx.send(embed=embed)

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to '{ctx.command}' command request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing '{ctx.command}' command. Error has already been reported to my developers."
            )
        except DBAPIError as err:
            self.bot.log.exception(
                f"Error processing database query for '{ctx.command}' command. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing '{ctx.command}' command. Error has already been reported to my developers."
            )
            session.rollback()
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to '{ctx.command}' command via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing '{ctx.command}' command. Error has already been reported to my developers."
            )
        finally:
            session.close()

    @commands.guild_only()
    @has_guild_permissions(manage_channels=True)
    @key.command(hidden=True)
    async def load(self, ctx, category: str = 'Default'):
        """Loads new product keys into the database."""

        session = self.bot.helpers.get_db_session()
        try:
            self.bot.log.info(
                f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})"
            )

            # Set meta data
            guild = ctx.message.guild
            db_guild = await self.bot.helpers.db_get_guild(session, guild.id)

            # Get all files attached
            all_files = ctx.message.attachments
            if not all_files:
                return await ctx.send(
                    f"No attachment found. Please be sure to upload an attachment containing the product keys to load into the database. File file should contain one key per line."
                )

            # Let the user know we're about to start.
            await ctx.send(
                f"Please wait while I process this list of keys. This could take a few minutes depending on the size of the list."
            )

            # Loads all keys from the file
            counter = 0
            dupe_count = 0
            for file in all_files:
                try:
                    data = io.BytesIO(await file.read())
                    error_count = 0
                    for key in data:
                        try:
                            key = str(key, "utf-8").strip()
                            new_key = models.Keys(
                                server=db_guild, product_key=key, category=category.lower()
                            )
                            session.add(new_key)
                            session.commit()
                            counter += 1
                            # If key adds to the database successfully, now add to the Redis cache
                            self.bot.helpers.redis.sadd(f"{category.lower()}_{guild.id}", f"{key}")
                            self.bot.log.info(f"Added key '{key}' from cat '{category}_{guild.id}' to database and Redis cache")
                        except IntegrityError as err:
                            session.rollback()
                            dupe_count += 1
                            self.bot.log.warning(
                                f"Database error loading key. Dupe key likely exists. {sys.exc_info()[0].__name__}: {err}"
                            )
                        except DBAPIError as err:
                            with configure_scope() as scope:
                                scope.set_extra("prod_key", key)
                                scope.set_extra("filename", file.filename)
                                scope.set_extra("d_user_id", ctx.message.author.id)
                                scope.set_extra("d_msg_id", ctx.message.id)
                            self.bot.log.exception(
                                f"Database error loading key. {sys.exc_info()[0].__name__}: {err}"
                            )
                            error_count += 1
                            if error_count >= 3:
                                break

                except Exception as err:
                    with configure_scope() as scope:
                        scope.set_extra("filename", file.filename)
                        scope.set_extra("d_user_id", ctx.message.author.id)
                        scope.set_extra("d_msg_id", ctx.message.id)
                    self.bot.log.exception(
                        f"Error downloading and processing the file. {sys.exc_info()[0].__name__}: {err}"
                    )

            return await ctx.send(
                f"Loaded __{counter}__ keys for **{category if category else 'default'}** category. There were {dupe_count} duplicates not loaded."
            )

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to '{ctx.command}' command request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing '{ctx.command}' command. Error has already been reported to my developers."
            )
        except DBAPIError as err:
            self.bot.log.exception(
                f"Error processing database query for '{ctx.command}' command. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing '{ctx.command}' command. Error has already been reported to my developers."
            )
            session.rollback()
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to '{ctx.command}' command via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing '{ctx.command}' command. Error has already been reported to my developers."
            )
        finally:
            session.close()


def setup(bot):
    bot.add_cog(Key(bot))
