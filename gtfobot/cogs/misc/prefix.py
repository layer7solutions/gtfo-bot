from discord.ext import commands
import discord
import sys
import json


class Prefix(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.guild_only()
    @commands.has_permissions(send_messages=True)
    @commands.group(invoke_without_command=True)
    async def prefix(self, ctx):
        """Show currently available prefixes. Mentioning the bot will always be available.

        Requires Permission: Send Messages
        """
        try:
            self.bot.log.info(
                f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})"
            )

            # Get current prefixes
            prefixes = self.bot.get_guild_prefixes(ctx.message)
            prefixes.pop(1)
            # Tell the users the new prefixes
            embed = discord.Embed(
                title="Current Prefixes", colour=discord.Colour.blurple()
            )
            embed.set_footer(text=f"{len(prefixes)} prefixes")
            embed.description = "\n".join(
                f"{index}: {elem}" for index, elem in enumerate(prefixes, 1)
            )
            await ctx.send(embed=embed)

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing {ctx.command}. Error has already been reported to my developers."
            )
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing {ctx.command}. Error has already been reported to my developers."
            )

    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @prefix.command(aliases=["a", "A"])
    async def add(self, ctx, newprefix: str):
        """Adds a prefix to the available server specific list. Mentioning the bot will always be available.

        To add a multi word prefix, or add a space to the end of the prefix allowing for "prefix command " instead of "prefixcommand" use double quotes `"` to surround the new prefix.

        Requires Permission: Manage Guild"""

        session = self.bot.helpers.get_db_session()
        try:
            self.bot.log.info(
                f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})"
            )

            # Get current prefixes stored from the database
            guild_settings = await self.bot.helpers.get_one_guild_settings(
                session, ctx.message.guild.id
            )

            # Convert from string to list
            if type(guild_settings.bot_prefix) is not list:
                guild_settings.bot_prefix = json.loads(guild_settings.bot_prefix)

            # If the prefix list is null/broken, start fresh
            if guild_settings.bot_prefix is None:
                guild_settings.bot_prefix = []

            if newprefix not in guild_settings.bot_prefix:
                await self.bot.add_guild_prefixes(
                    session, ctx.message.guild.id, newprefix, guild_settings
                )

                # Get current prefixes
                prefixes = self.bot.get_guild_prefixes(ctx.message)
                prefixes.pop(1)
                # Tell the users the new prefixes
                embed = discord.Embed(
                    title="Current Prefixes", colour=discord.Colour.blurple()
                )
                embed.set_footer(text=f"{len(prefixes)} prefixes")
                embed.description = "\n".join(
                    f"{index}: {elem}" for index, elem in enumerate(prefixes, 1)
                )
                await ctx.send(embed=embed)
            else:
                await ctx.send(
                    f"That prefix already exists. Use `<@!{self.bot.user.id}> prefix` command to see current prefixes available."
                )

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing {ctx.command}. Error has already been reported to my developers."
            )
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing {ctx.command}. Error has already been reported to my developers."
            )
        finally:
            session.close()

    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @prefix.command(aliases=["r", "R", "d", "D"])
    async def remove(self, ctx, oldprefix: str):
        """Removes a prefix from the available server specific list. Mentioning the bot will always be available.

        To remove a multi word prefix use double quotes `"` to surround the prefix.

        Requires Permission: Manage Guild"""

        session = self.bot.helpers.get_db_session()
        try:
            self.bot.log.info(
                f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})"
            )

            # Get current prefixes stored from the database
            guild_settings = await self.bot.helpers.get_one_guild_settings(
                session, ctx.message.guild.id
            )
            # Convert from string to list
            if type(guild_settings.bot_prefix) is not list:
                guild_settings.bot_prefix = json.loads(guild_settings.bot_prefix)

            # If there are no prefixes, don't continue
            if guild_settings.bot_prefix is None:
                return await ctx.send(
                    f"You do not have any custom prefixes. Try '<@!{self.bot.user.id}> prefix add' command to add one."
                )

            # Make sure prefix is in the list before modifications
            if oldprefix in guild_settings.bot_prefix:
                await self.bot.remove_guild_prefixes(
                    session, ctx.message.guild.id, oldprefix, guild_settings
                )

                # Get current prefixes
                prefixes = self.bot.get_guild_prefixes(ctx.message)
                prefixes.pop(1)
                # Tell the users the new prefixes
                embed = discord.Embed(
                    title="Current Prefixes", colour=discord.Colour.blurple()
                )
                embed.set_footer(text=f"{len(prefixes)} prefixes")
                embed.description = "\n".join(
                    f"{index}: {elem}" for index, elem in enumerate(prefixes, 1)
                )
                await ctx.send(embed=embed)
            else:
                await ctx.send(
                    f"The prefix `{oldprefix}` was not found in the list. Use `<@!{self.bot.user.id}> prefix` command to see current prefixes available."
                )
        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing {ctx.command}. Error has already been reported to my developers."
            )
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing {ctx.command}. Error has already been reported to my developers."
            )
        finally:
            session.close()


def setup(bot):
    bot.add_cog(Prefix(bot))
