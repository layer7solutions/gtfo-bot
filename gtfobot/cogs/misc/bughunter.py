import sys
import json
import asyncio

import discord
import requests
from atlassian import Jira as jira_api
from discord.ext import commands


class BugHunter(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.jira_user_mgmt_url = "https://admin.atlassian.com/s/e8e3f75c-c0c9-4fa7-b514-80c4905f3161"
        # Jira values Layer 7
        # self.uname = self.bot.botconfig.get("JiraL7", "USERNAME")
        # self.pword = self.bot.botconfig.get("JiraL7", "PASSWORD")
        # self.server_url = self.bot.botconfig.get("JiraL7", "SERVERURL")
        # self.project = self.bot.botconfig.get("JiraL7", "PROJECT")
        # self.project_id = self.bot.botconfig.get("JiraL7", "PROJECT_ID")

        # Jira values 10 Chambers
        self.tenc_uname = self.bot.botconfig.get("Jira10c", "USERNAME")
        self.tenc_pword = self.bot.botconfig.get("Jira10c", "PASSWORD")
        self.tenc_server_url = self.bot.botconfig.get("Jira10c", "SERVERURL")
        self.jira_auth = requests.auth.HTTPBasicAuth(self.tenc_uname,
                                                     self.tenc_pword)
        self.jira_headers = {
            "Accept": "application/json",
            "Content-Type": "application/json"
        }
        # Bug Hunter
        self.bh_project = self.bot.botconfig.get("Jira10c", "BH_PROJECT")
        self.bh_project_id = self.bot.botconfig.get("Jira10c", "BH_PROJECT_ID")
        # Dev
        self.dev_project = self.bot.botconfig.get("Jira10c", "DEV_PROJECT")
        self.dev_project_id = self.bot.botconfig.get("Jira10c", "DEV_PROJECT_ID")

        # Playfab values
        self.playfab_ds_secret = self.bot.botconfig.get("Playfab", "SECRET")
        self.playfab_ds_baseapi = self.bot.botconfig.get("Playfab", "BASEAPI")
        # Login to Jira
        try:
            # self.jira = jira_api(url=self.server_url, username=self.uname,
            # password=self.pword)
            self.tenc_jira = jira_api(
                url=self.tenc_server_url,
                username=self.tenc_uname,
                password=self.tenc_pword,
                cloud=True
            )
        except Exception as err:
            self.bot.log.exception(f"Failed to login to Jira. Error: {err}")

    @commands.command(aliases=["v"])
    @commands.has_permissions(administrator=True)
    @commands.guild_only()
    async def version(self, ctx, version: str):
        """Creates a new version in the Jira Kanban Board for the GTFO project.

        Requires Permission: Administrator

        Examples:
            version 123456
        """
        try:
            self.bot.log.info(
                f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})"
            )

            # Create version on Bug Hunter Board
            new_bh_version = await self.jira_create_version(self.tenc_jira, version,
                                                            self.bh_project,
                                                            self.bh_project_id)
            if type(new_bh_version) == bool:
                await ctx.send(f"Version already exists")
            elif new_bh_version:
                await ctx.send(
                    f"Successfully created new version called: *"
                    f"*{new_bh_version['name']}** in project {self.bh_project}"
                )
            else:
                await ctx.send(
                    f"Failed to create a new version on the {self.bh_project} board.")

            # Create version on Dev Board
            new_dev_version = await self.jira_create_version(self.tenc_jira, version,
                                                             self.dev_project,
                                                             self.dev_project_id)
            if type(new_dev_version) == bool:
                await ctx.send(f"Version already exists")
            elif new_dev_version:
                await ctx.send(
                    f"Successfully created new version called: *"
                    f"*{new_dev_version['name']}** in project {self.dev_project}"
                )
            else:
                await ctx.send(
                    f"Failed to create a new version on the {self.dev_project} board.")

        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing {ctx.command}. Error has already been reported to my developers."
            )
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing {ctx.command}. Error has already been reported to my developers."
            )

    async def jira_create_version(self, jira_obj, version, project_key, project_id):
        try:
            # Create version on Dev Board
            new_version = jira_obj.add_version(
                version=version,
                project_key=project_key,
                project_id=project_id,
            )
            if new_version:
                return new_version
        except requests.HTTPError:
            return True
        except Exception as err:
            self.bot.log.exception(
                f"Failed to create a new version on the Dev Jira board."
                f" {sys.exc_info()[0].__name__}: {err}"
            )
            return False

    @commands.command(aliases=["lv", "listversions"])
    @commands.has_permissions(administrator=True)
    @commands.guild_only()
    async def listversion(self, ctx):
        """Lists all versions in the Jira Kanban Board for the GTFO project.

        Requires Permission: Administrator

        Examples:
            listversion
            lv
        """
        try:
            self.bot.log.info(
                f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})"
            )
            try:
                # Get versions
                gtfo_versions = self.tenc_jira.get_project_versions(
                    key=self.bh_project)

                if gtfo_versions:
                    versions_temp = []
                    for version in gtfo_versions:
                        if version["archived"]:
                            continue
                        versions_temp.append(version["name"])
                    versions_formatted = "\n".join(versions_temp)
                    await ctx.send(
                        f"The following versions are found in the {self.bh_project} "
                        f"board:\n\n{versions_formatted}"[:1800]
                    )
            except Exception as err:
                self.bot.log.exception(
                    f"Failed to get/send list of versions. {sys.exc_info()[0].__name__}: {err}"
                )

        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing {ctx.command}. Error has already been reported to my developers."
            )
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing {ctx.command}. Error has already been reported to my developers."
            )

    @commands.command(aliases=["bhadd"])
    @commands.has_permissions(administrator=True)
    @commands.guild_only()
    async def addbh(self, ctx, user_id: int, email: str, steam_id: int):
        """Creates a new user in Jira and enables PlayFab Bug Hunter access.

        Requires Permission: Administrator

        Examples:
            addbh discord_id email steam_id
            addbh 123 test@example.com 789
        """
        try:
            self.bot.log.info(
                f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})"
            )
            # TO DO - Disabled until can figure out how to create a user in 10c Cloud site
            # return await ctx.send(f"This command currently disabled.")

            # Get the user from ID
            user = await self.bot.helpers.get_member_or_user(user_id, ctx.message.guild)
            if not user:
                return await ctx.send(
                    f"Unable to find the requested user. Please make sure the user ID or @ mention is valid."
                )

            bh_group = "GTFO Bug Hunter"
            bug_hunter_discord_channel = 635880894342889502
            bug_hunter_discord_role = 635876427681759262
            new_jira_username = str(user)
            jira_account_id = None
            jira_user = None
            jira_created_user = "\N{CROSS MARK}"
            jira_group_added = "\N{CROSS MARK}"
            playfab_access_added = "\N{CROSS MARK}"
            discord_role_added = "\N{CROSS MARK}"

            # Create the user in Jira
            jira_user_obj = None
            try:
                jira_user_obj = self.tenc_jira.user_create(
                    username=new_jira_username,
                    email=email,
                    display_name=new_jira_username,
                    password=None,
                    notification=True,
                )
                # If user is created
                if jira_user_obj:
                    self.bot.log.info(
                        f"Jira User {new_jira_username} should be created: {jira_user_obj}"
                    )
                    jira_account_id = jira_user_obj["accountId"]
                jira_created_user = "\N{WHITE HEAVY CHECK MARK}"
            except requests.HTTPError as err:
                self.bot.log.warning(
                    f"Failed to create new Jira user {new_jira_username}. User likely already exists. {sys.exc_info()[0].__name__}"
                )
                # Attempt to find user:
                jira_user_search = self.tenc_jira.user_find_by_user_string(query=email,
                                                                           start=0,
                                                                           limit=1,
                                                                           include_inactive_users=True,
                                                                           include_active_users=True)
                if jira_user_search:
                    new_jira_username = jira_user_search[0]['displayName']
                    jira_account_id = jira_user_search[0]["accountId"]
                    jira_user_obj = self.tenc_jira.user(account_id=jira_account_id,
                                                        expand="groups,applicationRoles")
                    self.bot.log.debug(f"Found Jira user: {jira_user_obj}")
                    jira_created_user = "\N{WHITE HEAVY CHECK MARK}"
            except Exception as err:
                self.bot.log.exception(
                    f"Unknown Exception creating new Jira user {new_jira_username}. jira_user_obj: {jira_user_obj} | {sys.exc_info()[0].__name__}:{err}"
                )

            # If user is created, do Jira tasks
            if jira_created_user:
                # Add to the Bug Hunters Group
                group_result = None
                try:
                    # Manual request to add user to group as the Jira API
                    # library we're using doesn't support via accountId yet
                    query = {
                        'groupname': bh_group
                    }
                    payload = json.dumps({
                        "accountId": jira_account_id
                    })
                    group_result = requests.request(
                        "POST",
                        f"{self.tenc_server_url}/rest/api/3/group/user",
                        data=payload,
                        headers=self.jira_headers,
                        params=query,
                        auth=self.jira_auth
                    )
                    # group_result = self.tenc_jira.add_user_to_group(
                    #    username=new_jira_username, group_name=bh_group
                    # )
                    if group_result:
                        # TO DO - This should check the error code. Currently it shows
                        # <Response [201]>
                        print(group_result)
                        jira_group_added = "\N{WHITE HEAVY CHECK MARK}"
                except requests.HTTPError as err:
                    self.bot.log.warning(
                        f"Failed to add to BH Group for Jira user {new_jira_username}. User likely already in the group.")
                    try:
                        # TO DO - Probably check the user first before sending add req
                        if jira_user_obj:
                            temp_groups = jira_user_obj['groups']
                            if temp_groups['size'] > 0:
                                for item in temp_groups['items']:
                                    if item['name'] == bh_group:
                                        jira_group_added = "\N{WHITE HEAVY CHECK MARK}"
                                        break
                    except Exception as err:
                        self.bot.log.warning(
                            f"Error getting Jira groups for user {new_jira_username}: {sys.exc_info()[0].__name__}: {err}")
                except Exception as err:
                    self.bot.log.exception(
                        f"Failed to add to BH Group for Jira user {new_jira_username}. {group_result} | {sys.exc_info()[0].__name__}: {err}"
                    )

            # Now let's give them the Bug Hunter role
            try:
                role = ctx.message.guild.get_role(bug_hunter_discord_role)
                if role:
                    if isinstance(user, discord.Member):
                        await user.add_roles(role)
                        discord_role_added = "\N{WHITE HEAVY CHECK MARK}"
                        self.bot.log.debug(f"Added BH role to user {user} ({user.id})")
                else:
                    self.bot.log.warning(
                        f"Unable to add BH role to user {user} ({user.id}), role not found")
            except Exception as err:
                self.bot.log.exception(
                    f"Failed to add role to user {user} ({user.id}). {sys.exc_info()[0].__name__}: {err}")

            # Now let's grant Playfab Access
            try:
                # First, let's check if they are a tester so we can avoid errors
                status = await self.manage_testers(steam_id=steam_id, action="istester")
                if status is True:
                    playfab_access_added = "\N{WHITE HEAVY CHECK MARK}"
                # If False or unknown, add them
                elif status is None or status is False:
                    status = await self.manage_testers(steam_id=steam_id,
                                                       action="addtester")
                    if status is True:
                        playfab_access_added = "\N{WHITE HEAVY CHECK MARK}"
            except Exception as err:
                self.bot.log.exception(
                    f"Failed to add Playfab Access for user {user} ({user.id}) | Steam ID: {steam_id}. {sys.exc_info()[0].__name__}: {err}")

            # Now let's send the final status update:
            await ctx.send(
                f"Add Bug Hunter Work Request Completed. Status Report:\n\n"
                f"{jira_created_user} **Jira Account Created:** {new_jira_username}\n"
                f"{jira_created_user} **Jira Account ID:** {jira_account_id} \n"
                f"{jira_group_added} **Jira BH Group Added**\n"
                f"{playfab_access_added} **Playfab Access Added**\n"
                f"{discord_role_added} **Discord Role Added**\n\n"
                f"**Account Link:** <{self.jira_user_mgmt_url}/users/{jira_account_id}>"
            )
            # Let's also welcome them to the server
            bh_channel = self.bot.get_channel(bug_hunter_discord_channel)
            if bh_channel:
                await bh_channel.send(
                    f"Welcome to the Bug Hunting team {user.mention}! We've created a Jira account for you, please check your email. Please take a look at <#688479201384464418> and <#688391057494638608> to get started. Let us know if you have any questions.")

        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing {ctx.command}. Error has already been reported to my developers."
            )
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing {ctx.command}. Error has already been reported to my developers."
            )

    @commands.command()
    @commands.has_permissions(administrator=True)
    @commands.guild_only()
    async def tester(self, ctx, action: str, steam_ids: str):
        """Manages a user's Bug Hunter group access in Playfab. Can also take multiple testers by a CSV list.

        Requires Permission: Administrator

        Examples:
            tester add 123
            tester remove 123
            tester check 123
            tester add 123,456,789

        """
        steam_id = None

        # Split the string to a list of IDs
        all_steam_ids = steam_ids.split(",")
        for steam_id in all_steam_ids:
            self.bot.log.info(f"BugHunters: Processing SteamID: {steam_id}")
            try:
                try:
                    steam_id = int(steam_id)
                except Exception:
                    # Checks that the steam_id can be converted to an int correctly
                    await ctx.send(
                        f"\N{WHITE QUESTION MARK ORNAMENT} Error converting Steam ID '{steam_id}' to an integer. Likely an invalid ID")
                    continue

                # Check if they are a tester
                if action == "check":
                    status = await self.manage_testers(steam_id=steam_id,
                                                       action="istester")
                    # Validate the results
                    if status is True:
                        await ctx.send(
                            f"\N{WHITE HEAVY CHECK MARK} That Steam ID '{steam_id}' **IS** a tester")
                    elif status is False:
                        await ctx.send(
                            f"\N{CROSS MARK} That Steam ID '{steam_id}' is **NOT** a tester")
                    else:
                        await ctx.send(
                            f"\N{WHITE QUESTION MARK ORNAMENT} Unknown status on that Steam ID '{steam_id}'")
                # Add as a tester
                elif action == "add":
                    action_type = "addtester"
                    status = await self.manage_testers(steam_id=steam_id,
                                                       action="istester")
                    # Validate the results
                    if status is True:
                        await ctx.send(
                            f"\N{WHITE HEAVY CHECK MARK} That Steam ID '{steam_id}' **IS** a tester, skipping")
                    elif status is None or status is False:
                        status = await self.manage_testers(steam_id=steam_id,
                                                           action=action_type)
                        # Validate the results
                        if status is True:
                            await ctx.send(
                                f"\N{WHITE HEAVY CHECK MARK} That Steam ID '{steam_id}' **IS** now a tester")
                        elif status is None or status is False:
                            await ctx.send(
                                f"\N{CROSS MARK} Failed to add that Steam ID '{steam_id}' as a tester")
                # Remove from being a tester
                elif action == "remove":
                    action_type = "removetester"
                    status = await self.manage_testers(steam_id=steam_id,
                                                       action="istester")
                    # Validate the results
                    if status is True:
                        # If they are a tester, now let's remove them
                        status = await self.manage_testers(steam_id=steam_id,
                                                           action=action_type)
                        # Validate the results
                        if status is True:
                            await ctx.send(
                                f"\N{WHITE HEAVY CHECK MARK} That Steam ID '{steam_id}' **NO LONGER** a tester")
                        elif status is None or status is False:
                            await ctx.send(
                                f"\N{CROSS MARK} Failed to remove that Steam ID '{steam_id}' from being a tester")
                    elif status is False:
                        await ctx.send(
                            f"\N{CROSS MARK} That Steam ID '{steam_id}' is **NOT** a tester")
                    elif status is None:
                        await ctx.send(
                            f"\N{WHITE QUESTION MARK ORNAMENT} Unknown status on that Steam ID '{steam_id}'")
                else:
                    return await ctx.send(
                        f"Sorry, that action type of {action} is unknown. Supported: add, remove, check")

            except Exception as err:
                self.bot.log.exception(
                    f"API to Playfab Failed for user Steam ID: {steam_id}. {sys.exc_info()[0].__name__}: {err}")
            # After each steam_id we're going to wait a bit to not break any unknown api limits (in seconds)
            await asyncio.sleep(1)

    async def manage_testers(self, steam_id: int, action: str):
        # Setting to None if we don't know anything
        status = None
        # Set header
        headers = {'X-SecretKey': self.playfab_ds_secret,
                   'content-type': 'application/json'}
        try:
            if action == "addtester":
                # Adds a tester to the Bug Hunter group
                data = {"SteamId": f"{steam_id}"}
                response = requests.post(
                    f"{self.playfab_ds_baseapi}/{self.bot.constants.gtfo_dropserver_endpoints[action]}",
                    headers=headers, json=data)
                # Note, for response.text we expect to see as type string = '{"Data":{}}' however an error would show if there was a problem
                self.bot.log.info(
                    f"BugHunters: addtester for {steam_id} | response: {response.text}")
                if response.status_code == 200:
                    # True means the action requested was successful, not their status after the call.
                    # No data gets return from the call if successful so we can only assume if no error then it was successful
                    status = True
                else:
                    status = False
            elif action == "removetester":
                data = {"SteamId": f"{steam_id}"}
                response = requests.post(
                    f"{self.playfab_ds_baseapi}/{self.bot.constants.gtfo_dropserver_endpoints[action]}",
                    headers=headers, json=data)
                if response.status_code == 200:
                    # True means the action requested was successful, not their status after the call.
                    # No data gets return from the call if successful so we can only assume if no error then it was successful
                    status = True
                else:
                    status = False
            elif action == "istester":
                data = {"SteamId": f"{steam_id}"}
                response = requests.post(
                    f"{self.playfab_ds_baseapi}/{self.bot.constants.gtfo_dropserver_endpoints[action]}",
                    headers=headers, json=data)
                self.bot.log.info(
                    f"BugHunters: istester check for {steam_id} | response: {response.text}")
                if response.status_code == 200:
                    result = json.loads(response.text)
                    status = result['Data']['IsTester']
                else:
                    # A status of None means we don't know, the call failed.
                    status = None
            else:
                # Returning None if we don't match
                status = None
        except Exception as err:
            self.bot.log.exception(
                f"Error handling Playfab DropServer Tester Requests. Action: {action}. {sys.exc_info()[0].__name__}: {err}")
            status = None
        finally:
            return status

    @commands.command()
    @commands.has_permissions(administrator=True)
    @commands.guild_only()
    async def invitebh(self, ctx, user_ids: str):
        """Invites a/many Bug Hhunters via a CSV list. Sends a pre-saved message that we normally send via email asking them to sign the NDA, etc.

        Requires Permission: Administrator

        Examples:
            invitebh 123
            invitebh 123,456,789

        """
        user_id = None

        # Split the string to a list of IDs
        all_ids = user_ids.split(",")
        for user_id in all_ids:
            self.bot.log.info(f"BugHunters: Processing UserID: {user_id}")
            try:
                user_id = int(user_id)
                user = await self.bot.helpers.get_member_or_user(user_id)
                await user.send(self.bot.constants.bug_hunter_invite_msg)
                await ctx.send(f"\N{WHITE HEAVY CHECK MARK} Sent invite message to {user} ({user.id})")
            except Exception as err:
                # Checks that the steam_id can be converted to an int correctly
                await ctx.send(
                    f"\N{CROSS MARK} Error sending to User ID '{user_id}', error: {err}")
                continue
            finally:
                # After each req we're going to sleep
                await asyncio.sleep(1)


def setup(bot):
    bot.add_cog(BugHunter(bot))
