# All the bots constants and reusable info stored here
from discord import ActivityType
from gql import gql
import re


__owner__ = "Mike Wohlrab aka D0cR3d#0001 (82942340309716992)"
__version__ = "0.2.0"
__botname__ = "GTFOBot"
__description__ = (
    "A discord bot developed by Layer 7 Solutions ("
    "https://Layer7.Solutions). The bot is "
    "designed for the https://Discord.gg/GTFO server."
)


class Constants:
    def __init__(self):
        self.repository_link = (
            "https://bitbucket.org/layer7solutions/gtfo-bot/src/master/"
        )
        self.patreon_link = "https://www.patreon.com/Layer7"
        self.support_invite_link = "https://discord.gg/JCzdcPU"
        # Thanks to https://emailregex.com for this unbearable mess
        self.email_regex = re.compile(
            r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"
        )
        self.prod_bot_media_spam = 503_382_909_387_014_154

        self.gtfo_dropserver_endpoints = {
            "addtester": "api/adm/v3/addtester",
            "removetester": "api/adm/v3/removetester",
            "istester": "api/adm/v3/istester",
        }

        self.pulsar_gql_store_query = gql(
            """
        mutation TenCCStore($dataType: Int!, $interactions: [Interaction!]!, $searches: [String!]!) {
            storeInteraction(dataType: $dataType, interactions: $interactions, searches: $searches){
                errors
                message
                status
            }
        }"""
        )

        self.pulsar_gql_validate_query = gql(
            """
        mutation TenCCValidate($dataType: Int!, $interactions: [Interaction!]!, $searches: [String!]!) {
            validateInteraction(dataType: $dataType, interactions: $interactions, searches: $searches){
                errors
                message
                status
            }
        }"""
        )

        self.reactions = {
            "upvote": 514_265_944_894_472_193,
            "downvote": 514_265_940_817_346_604,
            "spacer": 328_352_361_569_583_105,  # also referred to as 'blank'
            "sweeperbot": 361_145_141_173_682_177,
            "animated_sweeperbot": 501_214_112_504_610_816,
            "checkmark": "\N{WHITE HEAVY CHECK MARK}",
            "guantlet": 443_192_427_587_567_617,
        }

        self.discord_official_invite_codes = [
            "discord-testers",  # Discord Testers/Bug Reporting server
            "events",  # Discord Events server
            "discord-feedback",  # Discord Feedback server
            "discord-api",  # Discord API Server
            "discord-linux",  # Discord Linux server
        ]

        self.statuses = {
            "online": "<:online:400626710262972416> Online",
            "idle": "<:away:400626681066291210> Away",
            "dnd": "<:dnd:400626641916657674> Do Not Disturb",
            "offline": "<:offline:400626796112117771> Offline or Invisible",
            "streaming": "<:streaming:400626732245450762> Streaming",
            "mobile": "<:Mobile:565663254362193925>",
        }

        self.activity_enum = {
            ActivityType.playing: "Playing",
            ActivityType.listening: "Listening",
            ActivityType.watching: "Watching",
            ActivityType.streaming: "Streaming",
            ActivityType.unknown: "Unknown",
            ActivityType.custom: "Custom Status",
        }

        self.log_channels = [
            "bot-logs",
            "deleted-logs",
            "member-logs",
            "name-change-logs",
            "voice-logs",
            "reaction-logs",
        ]

        self.bug_hunter_invite_msg = """Hello,

Congratulations on being selected for the GTFO Bug Hunting Program. Ludvig, D0cR3d & our Admin team will be your leaders through this program. Please be sure to perform the following action items:

1. As mentioned, an NDA is required to be signed, so please promptly sign the NDA with your **Full Real Name, Signature (cursive), & Address (Street Name and #, City, State/Zip, & Country).** This can be a digital signature (such as using <https://www.sejda.com/sign-pdf>) or printing & scanning.

NDA File: <https://layer7.xyz/0CyVgj>

2. **Once you have the NDA signed, please upload using the following form (<https://layer7.xyz/bj431f>).**

3. Please join the Bug Hunters Discord server ( https://discord.gg/2xaRKYa ) and be sure to read the Rules and Info.

4. Once we have verified your NDA, we will grant you access to the server and our Jira board which is used for logging and tracking of bugs.

*Please do read the Non-Disclosure Agreement in full. We do not tolerate any leaks. Consider everything you do or learn while in the Bug Hunting program, the Discord Server, and the Jira Board as confidential and cannot be shared. While you are playing you can only use the Bug Hunter voice channels. Absolutely nothing can be shared outside the server without Admin or Dev approval.*

Finally, we do require a bit of activity to stay in the program. This doesn't mean we're going to require 10 hours a day, but do put some time in verifying bugs on the Jira board, updating any repro steps or details, engaging in bug hunter conversations on the server, using our LFG channel to play with other bug hunters, etc. If there is no activity for a significant period of time then you risk being removed, however if you are going to be inactive, please message an Admin and we will discuss it with you.

If you have any questions feel free to ask D0cR3d, Ludvig, or any of our Admins.

Thanks,
D0cR3d
"""
