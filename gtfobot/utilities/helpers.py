import json
import sys

import discord
from discord.ext import commands
from mailchimp3 import MailChimp, helpers
from mailchimp3.mailchimpclient import MailChimpError
from sentry_sdk import configure_scope
from rediscluster import RedisCluster
from db import models
from datetime import datetime, timezone


class Helpers:
    def __init__(self, bot):
        self.bot = bot
        self.mc = MailChimp(mc_api=self.bot.mailchimp_api_key)
        # Disabled Redis as it's only used for the key.py system which isn't being used
        # Create the redis connection
        # redis_hosts = self.bot.botconfig.get("Redis", "HOST").split(",")
        # redis_password = self.bot.botconfig.get("Redis", "PASSWORD")
        # redis_database = self.bot.botconfig.get("Redis", "DATABASE")
        # node_list = []
        # for node in redis_hosts:
        #    temp_d = {"host": node, "port": "6379"}
        #    node_list.append(temp_d)

        # self.redis = RedisCluster(startup_nodes=node_list, decode_responses=True, password=redis_password)

    async def get_member_or_user(self, input_str: str, guild: discord.Guild = None):
        # Let's clean the input first (could be an ID or a mention)
        if type(input_str) != int:
            user_id = self.clean_mention(input_str)
        else:
            user_id = input_str
        # Let's try to get a member on the server
        if guild:
            if user_id:
                member = guild.get_member(user_id)
                if member:
                    return member
            else:
                member = discord.utils.find(
                    lambda mbr: str(mbr).lower() == input_str.lower(), guild.members
                )
                if member:
                    return member

        # Try to get a user from local cache first
        if user_id:
            user = self.bot.get_user(user_id)
            if user:
                return user
        else:
            user = discord.utils.find(
                lambda usr: str(usr).lower() == input_str, self.bot.users
            )
            if user:
                return user

        # If no user, fetch with an API call
        if user_id:
            try:
                user = await self.bot.fetch_user(user_id)
                if user:
                    return user
            except (discord.NotFound, discord.HTTPException):
                # No user found, pass
                pass
        # If we make it this far, return None
        return None

    def clean_mention(self, mention: str):
        """Takes a mention, and tries to return the underlying Discord ID"""

        mention_temp = mention
        # If the 5th character from the right is a pound symbol, then it's a name#discrim not a mention or Discord ID
        if len(mention) > 5 and mention[-5] == "#":
            return None
        # Clean the mention to be just an ID
        for char in ["<", "#", "&", "@", "!", ">"]:
            mention_temp = mention_temp.replace(char, "")

        try:
            return int(mention_temp)
        except ValueError:
            return None

    def relative_time(self, start_time, end_time, brief=False):
        delta = start_time - end_time
        hours, remainder = divmod(int(delta.total_seconds()), 3600)
        minutes, seconds = divmod(remainder, 60)
        days, hours = divmod(hours, 24)

        if not brief:
            if days:
                fmt = "{d:,} days, {h} hours, {m} minutes, and {s} seconds"
            else:
                fmt = "{h} hours, {m} minutes, and {s} seconds"
        else:
            fmt = "{m}m {s}s"
            if hours:
                fmt = "{h:,}h " + fmt
                if days:
                    fmt = "{d:,}d " + fmt

        return fmt.format(d=days, h=hours, m=minutes, s=seconds)

    def get_db_session(self, db_config="Database"):
        try:
            self.bot.log.debug(f"Getting DB session for DB: {db_config}")
            return self.bot.database.get_session(db_config)
        except Exception as err:
            self.bot.log.exception(f"Error getting session for DB: {db_config}. Error: {err}")
            return None

    def get_all_guild_settings(self):
        session = self.get_db_session()
        try:
            self.bot.log.info(f"Getting all guild settings")
            # Get all guild settings
            guild_settings = session.query(models.ServerSetting).all()

            all_settings = {}
            if guild_settings:
                for guild in guild_settings:
                    guild_id = (
                        session.query(models.Server)
                        .filter(models.Server.id == guild.server_id)
                        .first()
                    ).discord_id

                    # Convert from string to list
                    if type(guild.bot_prefix) is not list:
                        guild.bot_prefix = json.loads(guild.bot_prefix)

                    all_settings[guild_id] = guild

            self.bot.guild_settings = all_settings
        except Exception as err:
            self.bot.log.exception(
                f"Error getting all guild settings. {sys.exc_info()[0].__name__}: {err}"
            )
            session.rollback()
        finally:
            session.close()

    async def get_one_guild_settings(self, session, guild_id):
        try:
            # Get guild settings
            guild_settings = (
                session.query(models.ServerSetting)
                .join(models.Server)
                .filter(models.Server.discord_id == guild_id)
                .first()
            )
            if not guild_settings:
                # Check if there is a guild in the database already
                guild = (
                    session.query(models.Server)
                    .filter(models.Server.discord_id == guild_id)
                    .first()
                )
                # If not, add one
                if not guild:
                    guild = self.db_add_new_guild(guild_id)
                # Now that we have a guild, let's finally create the settings
                guild_settings = self.db_add_new_guild_settings(session, guild)

            # Convert from string to list
            if type(guild_settings.bot_prefix) is not list:
                guild_settings.bot_prefix = json.loads(guild_settings.bot_prefix)

            return guild_settings
        except Exception as err:
            self.bot.log.exception(
                f"Error getting guild setting for {guild_id}. {sys.exc_info()[0].__name__}: {err}"
            )
            session.rollback()

    async def db_add_new_guild(self, session, guild_id):
        try:
            new_guild = models.Server(discord_id=guild_id)
            session.add(new_guild)
            session.commit()
            return new_guild
        except Exception as err:
            self.bot.log.exception(
                f"Error adding new guild '{guild_id}' to database. {sys.exc_info()[0].__name__}: {err}"
            )
            session.rollback()

    def db_add_new_guild_settings(self, session, guild):
        try:
            new_settings = models.ServerSetting(server=guild)
            session.add(new_settings)
            session.commit()
            return new_settings
        except Exception as err:
            self.bot.log.exception(
                f"Error adding new guild settings '{guild.discord_id}' to database. {sys.exc_info()[0].__name__}: {err}"
            )
            session.rollback()
            raise

    async def db_get_user(self, session, discord_id):
        try:
            # Try and get record from database
            db_user = (
                session.query(models.User)
                .filter(models.User.discord_id == discord_id)
                .first()
            )
            # If no DB record for the user then create one
            if not db_user:
                db_user = models.User(discord_id=discord_id)
                session.add(db_user)
                session.commit()
            return db_user
        except Exception as err:
            self.bot.log.exception(
                f"Error getting / adding user to database: '{discord_id}'. {sys.exc_info()[0].__name__}: {err}"
            )

    async def db_get_guild(self, session, discord_id):
        try:
            # Try and get record from database
            db_guild = (
                session.query(models.Server)
                .filter(models.Server.discord_id == discord_id)
                .first()
            )
            # If no DB record for the guild, create one
            if not db_guild:
                db_guild = self.bot.helpers.db_add_new_guild(discord_id)
            return db_guild
        except Exception as err:
            self.bot.log.exception(
                f"Error getting / adding guild to database: '{discord_id}'. {sys.exc_info()[0].__name__}: {err}"
            )

    async def check_guild_avail(self, guild, author):
        if not guild or guild.unavailable:
            self.bot.log.debug(
                f"The guild {guild.name} is not found or unavailable. Msg from {author} ({author.id})"
            )
            return False
        else:
            return True

    async def check_if_amba_role_found(self, amba_role, guild, author):
        if not amba_role:
            self.bot.log.debug(
                f"Unable to find the role {self.bot.gtfo_role_id} in the guild {guild.name}. Msg from {author} ({author.id})"
            )
            return False
        else:
            return True

    async def check_if_user_has_amba_role(self, member, amba_role):
        if amba_role in member.roles:
            self.bot.log.debug(
                f"User {member} ({member.id}) already has the {amba_role.name} role."
            )
            return True
        else:
            self.bot.log.debug(
                f"User {member} ({member.id}) does NOT have the {amba_role.name} role."
            )
            return False

    def clean_mailchimp_error(self, error):
        return json.loads(str(error).replace("'", '"'))

    def clean_email(self, email):

        return

    async def check_mc_is_subscribed(self, email, msg):
        try:
            email_hash = helpers.get_subscriber_hash(email)
            user = self.mc.lists.members.get(
                list_id=self.bot.mailchimp_list_id,
                subscriber_hash=email_hash,
                fields="status",
            )
            if user and user["status"] == "subscribed":
                return True
            else:
                return False
        except MailChimpError as err:
            err = self.clean_mailchimp_error(err)
            if err["status"] == 404:
                self.bot.log.warning(f"User not found in MailChimp list.")
                return None
            else:
                with configure_scope() as scope:
                    scope.set_extra("email", email)
                    scope.set_extra("d_user_id", msg.author.id)
                    scope.set_extra("d_msg_id", msg.id)
                self.bot.log.exception(
                    f"MailChimp Error checking users subscription. {sys.exc_info()[0].__name__}: {err}"
                )
                return False
        except Exception as err:
            with configure_scope() as scope:
                scope.set_extra("email", email)
                scope.set_extra("d_user_id", msg.author.id)
                scope.set_extra("d_msg_id", msg.id)
            self.bot.log.exception(
                f"Generic Error checking users mailchimp subscription. {sys.exc_info()[0].__name__}: {err}"
            )
            return False

    def pulsar_serialize_data(self, created_at, message_id, message_body):
        data = None
        try:
            # Convert timestamp to DateTime
            utc_dt = datetime.utcfromtimestamp(created_at)
            aware_utc_dt = utc_dt.replace(tzinfo=timezone.utc)

            data = {
                "content": {
                    "publishedAt": f"{aware_utc_dt.isoformat()}",
                    "remoteId": f"{message_id}",
                    "body": json.dumps(message_body),
                }
            }
        except Exception as err:
            self.bot.log.exception(
                f"Error serializing pulsar data: {sys.exc_info()[0].__name__}: {err}"
            )
        finally:
            return data


def has_guild_permissions(**perms):
    def predicate(ctx):
        author = ctx.message.author
        permissions = author.guild_permissions

        missing = [
            perm
            for perm, value in perms.items()
            if getattr(permissions, perm, None) != value
        ]

        if not missing:
            return True

        raise commands.MissingPermissions(missing)

    return commands.check(predicate)
