import asyncio
import sys
from datetime import datetime, timezone, timedelta

from gql import Client  # https://gql.readthedocs.io/en/latest/
from gql.transport.requests import RequestsHTTPTransport
from sqlalchemy.exc import DBAPIError
from sqlalchemy import func

from db import models


class Tasks:
    def __init__(self, bot):
        self.bot = bot
        self.all_tasks = []
        self.loaded = None
        self.positive_inf = None
        self.negative_inf = None
        self.not_a_number = None
        # Pulsar
        self.PULSAR_GRAPHQL_PUSHER_URL = self.bot.botconfig.get(
            "Pulsar", "GRAPHQL_PUSHER_URL"
        )
        self.PULSAR_API_KEY = self.bot.botconfig.get("Pulsar", "API_KEY")
        self.PULSAR_SEARCH_HASH = self.bot.botconfig.get("Pulsar", "SEARCH_HASH")
        self.PULSAR_GUILD_ID = int(self.bot.botconfig.get("Pulsar", "GUILD_ID"))
        self.DB_ALLOWED_CHANNELS_TEMP = self.bot.botconfig.get("Pulsar",
                                                               "CHANNEL_IDS").split(",")
        self.PULSAR_REQUESTS_PER_SEC = int(
            self.bot.botconfig.get("Pulsar", "REQ_PER_SEC"))
        self.DB_ALLOWED_CHANNELS = []
        for item in self.DB_ALLOWED_CHANNELS_TEMP:
            self.DB_ALLOWED_CHANNELS.append(int(item))
        #
        self.pulsar_gql_transport = RequestsHTTPTransport(
            url=self.PULSAR_GRAPHQL_PUSHER_URL,
            verify=True,
            retries=3,
            headers={"Authorization": f"Bearer {self.PULSAR_API_KEY}"},
        )
        self.pulsar_gql_client = Client(transport=self.pulsar_gql_transport)

    async def start_tasks(self):
        # Internal check if we've already loaded and started the tasks
        if self.loaded:
            self.bot.log.info(f"Tasks: Already loaded start_tasks, skipping")
            return

        # Push data to Pulsar
        #task_push_to_pulsar = asyncio.create_task(self.push_to_pulsar())
        #self.all_tasks.append(task_push_to_pulsar)
        #self.bot.log.info(f"Tasks: Loaded: task_push_to_pulsar")

        # All done
        self.bot.log.info(
            f"Tasks: Done loading all tasks | Bot Startup: {self.bot.started_time.timestamp()}"
        )
        self.loaded = True

    async def push_to_pulsar(self):
        while True:
            # UTC Timezone Aware Now
            dt_now = datetime.now(timezone.utc)
            # UTC Timezone Aware Minus 2 Days
            dt_2_days_ago = dt_now - timedelta(days=2)
            # And as a timestamp int
            dt_2_days_ago_ts = int(dt_2_days_ago.timestamp())

            # Get DB Session to see how many unprocessed records
            session = self.bot.helpers.get_db_session()
            unprocessed_record_count = 0
            try:
                # Get records from DB
                record_count = (
                    session.query(
                        func.count(models.MessageData.id)
                    )
                    # Filters:
                    .filter(
                        # - Not exported
                        models.MessageData.exported == False,
                        # - Export hasn't failed
                        models.MessageData.export_failed == False,
                        # - Only Specific Guilds Data
                        models.MessageData.guild_id == self.PULSAR_GUILD_ID,
                        # - Only Specific Channels Data
                        models.MessageData.channel_id.in_(self.DB_ALLOWED_CHANNELS),
                        # - Older Than 2 Days
                        models.MessageData.created_at <= dt_2_days_ago_ts,

                    )
                    # Get all the results matching this query
                    .all()
                )
                unprocessed_record_count = record_count[0][0]
            except DBAPIError as err:
                self.bot.log.exception(
                    f"Database error getting unprocessed record count. {sys.exc_info()[0].__name__}: {err}")
                session.rollback()
            except Exception as err:
                self.bot.log.exception(
                    f"Generic error getting unprocessed record count. {sys.exc_info()[0].__name__}: {err}")
            finally:
                session.close()

            # If there are unprocessed records, then continue
            if unprocessed_record_count == 0:
                # If there are no unprocessed records, sleep 5 minutes
                # to avoid excess processing/queries
                await asyncio.sleep(60 * 5)

            while unprocessed_record_count > 0:
                processed_rows = []
                interactions = []
                # Get DB Session to process the records
                session = self.bot.helpers.get_db_session()
                try:
                    # Get records from DB
                    msg_data_results = (
                        session.query(models.MessageData)
                        # Filters:
                        .filter(
                            # - Not exported
                            models.MessageData.exported == False,
                            # - Export hasn't failed
                            models.MessageData.export_failed == False,
                            # - Only Specific Guilds Data
                            models.MessageData.guild_id == self.PULSAR_GUILD_ID,
                            # - Only Specific Channels Data
                            models.MessageData.channel_id.in_(self.DB_ALLOWED_CHANNELS),
                            # - Older Than 2 Days
                            models.MessageData.created_at <= dt_2_days_ago_ts,

                        )
                        # - Max 100 at a time
                        .limit(100)
                        # Get all the results matching this query
                        .all()
                    )

                    for result in msg_data_results:
                        # Important Variables
                        # result.id
                        # result.created_at
                        # result.message_id
                        # result.message_body
                        # result.export_failed
                        # Setting exported to true so we don't reprocess
                        result.exported = True

                        # Serialize the data
                        serialized_data = self.bot.helpers.pulsar_serialize_data(
                            created_at=result.created_at, message_id=result.message_id,
                            message_body=result.message_body)
                        # Append to interactions list
                        if serialized_data:
                            processed_rows.append(result)
                            interactions.append(serialized_data)
                            # Add to session to commit to the DB
                            session.add(result)

                    # Now that we're done going through all the results
                    # Create the overall dict to send
                    if interactions:
                        # Create the dict to send to Pulsar
                        gql_data = {
                            "dataType": 1,
                            "interactions": interactions,
                            "searches": [self.PULSAR_SEARCH_HASH],
                        }
                        result = self.pulsar_gql_client.execute(
                            self.bot.constants.pulsar_gql_store_query,
                            variable_values=gql_data)
                        self.bot.log.info(result)
                        # If there were errors w/ Pulsar API Query
                        if result['storeInteraction']['errors']:
                            for item in interactions:
                                # Indicate it was NOT exported, and export failed
                                item.exported = False
                                item.export_failed = True
                                # Add item to the session to commit
                                session.add(item)
                            # Log failed count
                            self.bot.log.info(
                                f"Failed Pulsar Record Count: {len(processed_rows)}")
                        else:
                            # Log success count
                            self.bot.log.info(f"Pushed items to Pulsar successfully: {len(processed_rows)}")

                        # Update the DB records
                        session.commit()
                        # Decrement the unprocessed_record_count
                        unprocessed_record_count -= len(processed_rows)
                        # Log that we're done with this iteration
                        self.bot.log.info(f"Done processing Pulsar records: {len(processed_rows)} | Remaining: {unprocessed_record_count}")

                except DBAPIError as err:
                    self.bot.log.exception(
                        f"Database error in Push to Pulsar Task. {sys.exc_info()[0].__name__}: {err}")
                    if interactions:
                        for item in interactions:
                            # Indicate it was NOT exported, and export failed
                            item.exported = False
                            item.export_failed = True
                    else:
                        session.rollback()
                except Exception as err:
                    self.bot.log.exception(
                        f"Generic error in Push to Pulsar Task. {sys.exc_info()[0].__name__}: {err}")
                    # If there's an error, wait 5 min
                    await asyncio.sleep(60 * 5)
                finally:
                    session.close()
                    # Time in seconds.
                    # round((1 second divided by requests per second) plus a buffer to not exceed the limit, rounded to 2 decimal places)
                    wait_time = round((1 / self.PULSAR_REQUESTS_PER_SEC) + .1, 2)
                    await asyncio.sleep(wait_time)

    async def cancel_all_tasks(self):
        for task in self.all_tasks:
            try:
                self.bot.log.info(f"Tasks: Attempting to cancel: {task}")
                task.cancel()
            except asyncio.CancelledError:
                self.bot.log.info(f"Tasks: Cancelled: {task}")
            except Exception as err:
                self.bot.log.exception(
                    f"Tasks: Not sure how this happened.. some error cancelling the task: {sys.exc_info()[0].__name__}: {err}"
                )
