import asyncio
import random
import sys
from datetime import datetime

import discord
from discord.ext import commands
from sqlalchemy.exc import DBAPIError, IntegrityError

from db import models


class Events(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_member_join(self, member):
        session = self.bot.helpers.get_db_session()
        try:
            # If the member joining is this bot then skip trying to log
            if self.bot.user.id == member.id:
                return

            # Log new user to the database
            # First check if they exist in our DB already
            dbuser = (
                session.query(models.User)
                .filter(models.User.discord_id == member.id)
                .first()
            )
            # If they don't exist in the database
            if not dbuser:
                # Add them to the database
                dbuser = models.User(discord_id=member.id)
                session.add(dbuser)
                session.commit()

            # -----------------------------
            # Finds any Alaric accounts and kicks from server
            if member.guild and member.guild.id == self.bot.gtfo_guild.id:
                if member.name.lower() in [
                    "chubbychaser",
                    "alaric",
                    "ysmir",
                    "ymir",
                    "gwendolyn",
                    "overkill_alaric",
                ]:
                    # Wait a random time between 5 and 60 seconds before kicking
                    # note: randrange doesn't return the last item in the range, i.e. it's exclusive
                    # therefore we need to say last range is 61, with a start of 5 and a step of 1 second
                    sleeptime = random.randrange(5, 61, 1)
                    await asyncio.sleep(sleeptime)
                    await member.guild.kick(member, reason="Possible Alaric Alt")
                    self.bot.log.info(
                        f"AUTO: Kicked possible Alaric alt from GTFO server. Name: '{member}' ({member.id})"
                    )
                    # Let's also let our mods know when Alaric is seen
                    mods_channel = discord.utils.get(
                        member.guild.text_channels, name="discord-mods"
                    )
                    if mods_channel:
                        await mods_channel.send(
                            f"Kicked possible Alaric alt from GTFO server. Name: '{member}' ({member.id})"
                        )

        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error processing Member Join Event. {sys.exc_info()[0].__name__}: {err}"
            )
        except Exception as err:
            self.bot.log.exception(
                f"Error processing Member Join Event. {sys.exc_info()[ 0].__name__}: {err}"
            )
        except DBAPIError as err:
            self.bot.log.exception(
                f"Error logging Member Join to database. {sys.exc_info()[0].__name__}: {err}"
            )
            session.rollback()
        finally:
            session.close()

    @commands.Cog.listener()
    async def on_guild_join(self, newguild):
        session = self.bot.helpers.get_db_session()
        try:
            # Check if there is a guild in the database already
            guild = (
                session.query(models.Server)
                .filter(models.Server.discord_id == newguild.id)
                .first()
            )

            # If not, add one
            if not guild:
                guild = await self.bot.helpers.db_add_new_guild(session, newguild.id)

            # Check if there are guild settings
            guild_settings = (
                session.query(models.ServerSetting)
                .filter(models.ServerSetting.server_id == guild.id)
                .first()
            )

            # If there is no guild settings, create them
            if not guild_settings:
                self.bot.helpers.db_add_new_guild_settings(session, guild)

            # Update local cache with new guild settings
            await self.bot.helpers.get_one_guild_settings(session, newguild.id)

            self.bot.log.info(f"Joined new guild: {newguild.name} ({newguild.id})")

            # Log in the bots support server
            guild = self.bot.get_guild(547_549_495_563_517_953)
            if guild:
                logs = discord.utils.get(guild.text_channels, name="guild-logs")
                if logs:
                    # Create the embed of info
                    embed = discord.Embed(
                        color=0x0B9CB4,
                        timestamp=datetime.utcnow(),
                        description=f"**Guild Description:** {newguild.description}",
                    )
                    embed.set_author(
                        name=f"Joined: '{newguild}' ({newguild.id})",
                        icon_url=newguild.icon_url,
                    )
                    embed.add_field(
                        name="Guild Owner",
                        value=f"{newguild.owner} ({newguild.owner.id})",
                        inline=False,
                    )
                    features = newguild.features if newguild.features else "None"
                    embed.add_field(name="Features", value=f"{features}", inline=False)
                    embed.add_field(
                        name="Total Members",
                        value=f"{newguild.member_count}",
                        inline=False,
                    )
                    embed.set_thumbnail(url=newguild.icon_url)
                    try:
                        await logs.send(embed=embed)
                    except discord.Forbidden:
                        self.bot.log.debug(
                            f"Missing permissions to send in {logs.name} ({logs.id}) in guild {logs.guild.name} ({logs.guild.id})"
                        )

        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error processing Guild Join Event. {sys.exc_info()[0].__name__}: {err}"
            )
        except Exception as err:
            self.bot.log.exception(
                f"Error processing Guild Join Event. {sys.exc_info()[0].__name__}: {err}"
            )
        except DBAPIError as err:
            self.bot.log.exception(
                f"Error with Guild Join Event database calls. {sys.exc_info()[0].__name__}: {err}"
            )
            session.rollback()
        finally:
            session.close()

    @commands.Cog.listener()
    async def on_guild_remove(self, oldguild):
        try:
            self.bot.log.info(f"Parted guild: {oldguild.name} ({oldguild.id})")

            # Log in the bots support server
            guild = self.bot.get_guild(547_549_495_563_517_953)
            if guild:
                logs = discord.utils.get(guild.text_channels, name="guild-logs")
                if logs:
                    # Create the embed of info
                    embed = discord.Embed(
                        color=0x06505C,
                        timestamp=datetime.utcnow(),
                        description=f"**Guild Description:** {oldguild.description}",
                    )
                    embed.set_author(
                        name=f"Parted: '{oldguild}' ({oldguild.id})",
                        icon_url=oldguild.icon_url,
                    )
                    embed.add_field(
                        name="Guild Owner",
                        value=f"{oldguild.owner} ({oldguild.owner.id})",
                        inline=False,
                    )
                    features = oldguild.features if oldguild.features else "None"
                    embed.add_field(name="Features", value=f"{features}", inline=False)
                    embed.add_field(
                        name="Total Members",
                        value=f"{oldguild.member_count}",
                        inline=False,
                    )
                    embed.set_thumbnail(url=oldguild.icon_url)
                    try:
                        await logs.send(embed=embed)
                    except discord.Forbidden:
                        self.bot.log.debug(
                            f"Missing permissions to send in {logs.name} ({logs.id}) in guild {logs.guild.name} ({logs.guild.id})"
                        )
        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error processing Guild Part Event. {sys.exc_info()[0].__name__}: {err}"
            )
        except Exception as err:
            self.bot.log.exception(
                f"Error processing Guild Part Event. {sys.exc_info()[0].__name__}: {err}"
            )

    @commands.Cog.listener()
    async def on_message(self, msg):
        # If from a bot, ignore
        if msg.author.bot:
            return

        # Check if it's from a DM, then ignore it since we're processing this in the mod mail module
        if isinstance(msg.channel, discord.DMChannel):
            return

        if isinstance(msg.channel, discord.TextChannel):
            # Log each message to the database
            # Unless it's in the prod-bot-media-spam channel which causes excessive logs
            if (
                self.bot.constants
                and msg.channel.id == self.bot.constants.prod_bot_media_spam
            ):
                return

            session = self.bot.helpers.get_db_session()
            try:
                message_details = {
                    "message_id": msg.id,
                    "guild_id": msg.guild.id,
                    "channel_id": msg.channel.id,
                    "created_at": msg.created_at.timestamp(),
                    "message_body": msg.content,
                }
                new_message = models.MessageData(
                    **message_details
                )
                session.add(new_message)
                session.commit()
            except IntegrityError as err:
                # We don't need to log when there is a duplicate message in the database as it won't be helpful.
                # self.bot.log.debug(
                #     f"Duplicate database record: '/{msg.guild.id}/{msg.channel.id}/{msg.id}' to database. {sys.exc_info()[0].__name__}: {err}"
                # )
                session.rollback()
            except DBAPIError as err:
                self.bot.log.exception(
                    f"Generic Error logging message '/{msg.guild.id}/{msg.channel.id}/{msg.id}' to database. {sys.exc_info()[0].__name__}: {err}"
                )
                session.rollback()
            finally:
                session.close()

    async def disabled_on_message_delete(self, message):
        if isinstance(message.channel, discord.DMChannel):
            return

        if not message.guild:
            return

        session = self.bot.helpers.get_db_session()
        try:
            # Get record
            message_db = (
                session.query(models.MessageData).filter(models.MessageData.message_id == message.id).first()
            )
            if message_db:
                session.delete(message_db)
                session.commit()

        except DBAPIError as err:
            self.bot.log.exception(
                f"Database error removing deleted message from database. {sys.exc_info()[0].__name__}: {err}"
            )
            session.rollback()
        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error processing Message Delete Event. {sys.exc_info()[0].__name__}: {err}"
            )
        except Exception as err:
            self.bot.log.exception(
                f"Error processing Message Delete Event. {sys.exc_info()[ 0].__name__}: {err}"
            )
        finally:
            session.close()

    async def on_raw_message_delete(self, raw_message):
        if not raw_message.guild_id:
            return

        session = self.bot.helpers.get_db_session()
        try:
            # Get record
            message_db = (
                session.query(models.MessageData).filter(models.MessageData.message_id == raw_message.message_id).first()
            )
            if message_db:
                session.delete(message_db)
                session.commit()

        except DBAPIError as err:
            self.bot.log.exception(
                f"Database error removing deleted message from database. {sys.exc_info()[0].__name__}: {err}"
            )
            session.rollback()
        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error processing Message Delete Event. {sys.exc_info()[0].__name__}: {err}"
            )
        except Exception as err:
            self.bot.log.exception(
                f"Error processing Message Delete Event. {sys.exc_info()[ 0].__name__}: {err}"
            )
        finally:
            session.close()


def setup(bot):
    bot.add_cog(Events(bot))
