"""This module defines the db models used by SweeperBot"""
import sqlalchemy
from sqlalchemy import (
    BigInteger,
    Boolean,
    Column,
    DateTime,
    ForeignKey,
    Integer,
    JSON,
    String,
)
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declared_attr, declarative_base
from citext import CIText


class SweeperBase(object):
    """This class provides the defaults for each class inheriting it."""

    @declared_attr
    def __tablename__(cls):
        """The table will be named a lower case version of its class name"""
        return cls.__name__.lower()

    id = Column(Integer, primary_key=True)
    created = Column(DateTime(timezone=True), server_default=sqlalchemy.sql.func.now())
    updated = Column(DateTime(timezone=True), onupdate=sqlalchemy.sql.func.now())


Base = declarative_base(cls=SweeperBase)


# Normal table models
class Server(Base):
    discord_id = Column(BigInteger, nullable=False, unique=True)


class User(Base):
    discord_id = Column(
        BigInteger, nullable=False, unique=True, comment="A user's unique discord id"
    )


class Message(Base):
    user_id = Column(Integer, ForeignKey("user.id"))
    user = relationship(User, backref=backref("message", uselist=True))
    server_id = Column(Integer, ForeignKey("server.id"))
    server = relationship(Server, backref=backref("message", uselist=True))
    channel_id = Column(BigInteger)
    channel_name = Column(String)
    message_id = Column(BigInteger)
    message_body = Column(String)


# Used for logging message for Pulsar
class MessageData(Base):
    message_id = Column(BigInteger, unique=True, index=True)
    guild_id = Column(BigInteger, index=True)
    channel_id = Column(BigInteger, index=True)
    created_at = Column(BigInteger, index=True)
    message_body = Column(String)
    exported = Column(Boolean, default=False, index=True)
    export_failed = Column(Boolean, default=False, index=True)


class DirectMessage(Base):
    # The Mod Mail Guild
    dm_server_id = Column(Integer, ForeignKey("server.id"))
    dm_server = relationship(Server, backref=backref("directmessage", uselist=True))
    # Channel ID and Message ID for that user in the Mod Mail guild
    dm_channel_id = Column(BigInteger)
    dm_message_id = Column(BigInteger)
    # User Channel ID and Message ID for the bots DMs
    user_channel_id = Column(BigInteger)
    user_message_id = Column(BigInteger)
    # User that message is associated with, could be the user or the mod
    user_id = Column(Integer, ForeignKey("user.id"))
    user = relationship(User, backref=backref("directmessage", uselist=True))
    # Message Body, could be user or the mod
    message_body = Column(String)
    # Whether message is inbound (from the user) or outbound (from the mod)
    inbound = Column(Boolean)


class ServerSetting(Base):
    server_id = Column(Integer, ForeignKey("server.id"))
    server = relationship(Server, backref=backref("serversetting", uselist=True))
    bot_prefix = Column(JSON, default='"[]"')
    modmail_server_id = Column(String)


class Tags(Base):
    server_id = Column(Integer, ForeignKey("server.id"), index=True)
    server = relationship(Server, backref=backref("tags", uselist=True))
    owner_id = Column(Integer, ForeignKey("user.id"))
    owner = relationship(User, backref=backref("tags", uselist=True))
    uses = Column(Integer, default=0)
    name = Column(CIText())
    content = Column(String)
    sqlalchemy.Index("tag_uniq_idx", name, server_id, unique=True)


class Keys(Base):
    """Represents a Guild and Product Key relationship.

    A product key is unique to the guild. We are only enforcing uniqueness on this
    which allows a user to redeem multiple keys if the guild allowed. Uniqueness
    would be enforced within the command prior to retrieval.

    Product keys would be loaded into the table prior to key retrieval. Required
    values for this would be the server_id, product_key. A category is not required
    by highly encouraged."""
    # Guild the key is for
    server_id = Column(Integer, ForeignKey("server.id"))
    server = relationship(Server, backref=backref("keys", uselist=True))
    # User that redeemed the key
    user_id = Column(Integer, ForeignKey("user.id"))
    user = relationship(User, backref=backref("keys", uselist=True))
    # The product key
    product_key = Column(String)
    # The campaign/category for the key. Ex alpha, beta
    category = Column(CIText())
    # Whether the key was redeemed
    redeemed = Column(Boolean, default=False)
    email = Column(CIText())

    sqlalchemy.Index("key_server_prodkey_uniq_idx", server_id, product_key, unique=True)


class Suggestions(Base):
    """Records the suggestions use make, and number of votes"""
    # Guild the suggestion is related to
    server_id = Column(Integer, ForeignKey("server.id"))
    server = relationship(Server, backref=backref("suggestions", uselist=True))
    # User that made the suggestion
    user_id = Column(Integer, ForeignKey("user.id"))
    user = relationship(User, backref=backref("suggestions", uselist=True))
    # ID of the message
    message_id = Column(BigInteger, unique=True)
    # Number of upvotes
    upvotes = Column(Integer, default=0)
    # Number of downvotes
    downvotes = Column(Integer, default=0)
    # The text of the suggestion
    text = Column(CIText())
