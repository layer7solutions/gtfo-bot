#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
from gtfobot.constants import __version__, __description__

with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("HISTORY.rst") as history_file:
    history = history_file.read()


setup(
    name="gtfobot",
    version=__version__,
    description=__description__,
    long_description=readme + "\n\n" + history,
    author="Mike Wohlrab",
    author_email="Mike@Layer7.Solutions",
    url="https://bitbucket.org/layer7solutions/gtfo-bot/",
    packages=find_packages(),
    entry_points={"console_scripts": ["gtfobot=gtfobot.launch:main"]},
    include_package_data=True,
    # license="GNU General Public License v3",
    zip_safe=False,
    keywords=[
        "gtfobot",
        "gtfo bot",
        "bot",
        "discord",
    ],
    classifiers=[
        "Development Status :: 2- Beta",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
    ],
)
